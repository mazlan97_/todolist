import React, {useState} from "react";
import { StyleSheet, View, TextInput, Button } from "react-native";
import {appStyles} from '../components/styles';
import Loader from '../components/loader';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function CreatePost({navigation}){
    
    const [postText, setPostText] = useState('');
    const [userId, setUserId] = useState('');
    const [loading, setLoading] = useState(false);
    
    AsyncStorage.getItem('user_id').then((user_id) => setUserId(user_id));
    const handleSubmitPress = () => {
        if (!postText) {
          alert('Please type something');
          return;
        }
        setLoading(true);
        let dataToSend = {caption: postText, user_id:userId};
        let formBody = [];
        for (let key in dataToSend) {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(dataToSend[key]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
    
        fetch('https://mazlan.dev/android_php/createPost.php', {
          method: 'POST',
          body: formBody,
          headers: {
            //Header Defination
            'Content-Type':
            'application/x-www-form-urlencoded;charset=UTF-8',
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            setLoading(false);
            if(responseJson.success){
                navigation.goBack();
            }
        })
        .catch((error) => {
            console.error(error);
            setLoading(false);
        })
    };
    
    return(
        <View style={styles.container}>
            <Loader loading={loading} />
            <View style={appStyles.container}>
                <TextInput 
                    multiline
                    placeholder="Type Anything" 
                    style={appStyles.textInput}
                    onChangeText={(text) => {setPostText(text)}}/>

                <View style={appStyles.button}>
                    <Button title="Create" onPress={handleSubmitPress}/>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding:30
    }
})