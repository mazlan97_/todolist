import React, {useState} from "react";
import { View, Text, TextInput, Button } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/loader';
import {appStyles} from '../components/styles';

export default function Login({navigation}){
    const [userId, setUserId] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    const handleSubmitPress = () => {
        if (!userId) {
          alert('Please fill UserId');
          return;
        }
        if (!password) {
          alert('Please fill Password');
          return;
        }
        setLoading(true);
        let dataToSend = {user_id: userId, password: password};
        let formBody = [];
        for (let key in dataToSend) {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(dataToSend[key]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
    
        fetch('https://mazlan.dev/android_php/login.php', {
          method: 'POST',
          body: formBody,
          headers: {
            //Header Defination
            'Content-Type':
            'application/x-www-form-urlencoded;charset=UTF-8',
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            
            AsyncStorage.getItem('login_key').then((login_key) => console.log(login_key));
            setLoading(false);
            if(responseJson.success){
                AsyncStorage.setItem('user_id', userId);
                AsyncStorage.setItem('login_key', responseJson.login_key);
                navigation.replace("MainStack");
            }
        })
        .catch((error) => {
            console.error(error);
            setLoading(false);
        })
    };

    return(
        <View style={appStyles.mainContainer}>
            <View style={appStyles.container}>
                <Loader loading={loading} />
                <Text style={appStyles.textLogo}>ToDo List</Text>
                <TextInput 
                    placeholder="User ID" 
                    style={appStyles.textInput}
                    onChangeText={(text) => {setUserId(text)}}/>

                <TextInput 
                    placeholder="Password" 
                    style={appStyles.textInput}
                    onChangeText={(text) => {setPassword(text)}}/>

                <View style={appStyles.button}>
                    <Button title="Login" onPress={handleSubmitPress}/>
                </View>
                {/* <View style={appStyles.button}>
                    <Button title="Register" onPress={() => navigation.navigate('Register')}/>
                </View> */}
            </View>
        </View>
    )
}

