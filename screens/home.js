import React, {useState, Component} from "react";
import { View, Text, FlatList, RefreshControl, StyleSheet, Image, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/loader';
import {appStyles} from '../components/styles';
import Card from '../components/card';
import { MaterialIcons } from '@expo/vector-icons';

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}
  
export default function Home({navigation}){
    const [userId, setUserId] = useState('');
    AsyncStorage.getItem('user_id').then((user_id) => setUserId(user_id));
    React.useLayoutEffect(() => {
        navigation.setOptions({
          headerRight: () => (
            <MaterialIcons 
            name="add"
            color={'#FFF'}
            style={{fontSize:25}}
            onPress={() => navigation.navigate("CreatePost")}
        />
          ),
          headerLeft: () => (
            <MaterialIcons 
            name="logout"
            color={'#FFF'}
            style={{fontSize:20}}
            onPress={LogOut}
        />
          )
        });
      }, [navigation]);
    const [refreshing, setRefreshing] = React.useState(false);


    const onRefresh = React.useCallback(() => {
      loadData();
      wait(5000).then(() => setRefreshing(false));
    }, []);
    
    const [loading, setLoading] = useState(false);
    const LogOut = () =>{
        setLoading(true);
        AsyncStorage.clear();
        navigation.replace("Auth");
    }
    const [posts, setPosts] = useState([]);


    const loadData = () => {
        setRefreshing(true);
        let dataToSend = {user_id: userId, user_idp: userId};
        let formBody = [];
        for (let key in dataToSend) {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(dataToSend[key]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
    
        fetch('https://mazlan.dev/android_php/getUPosts.php', {
          method: 'POST',
          body: formBody,
          headers: {
            //Header Defination
            'Content-Type':
            'application/x-www-form-urlencoded;charset=UTF-8',
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            
            setPosts(responseJson.posts);
            setRefreshing(false);
        })
        .catch((error) => {
            console.error(error);
            setRefreshing(false);
        })
    };
    if(posts.length == 0 && !refreshing){
        loadData();
    }
    function viewBanner(banner){
        if(banner == "")
            return;
        else 
            return (
                <View style={{backgroundColor:'#CCC', flex:1, alignSelf:'stretch', height:200,borderRadius:10, elevation:4}} >

                <Image
                        style={{width:null, height:null, flex:1,borderRadius:10}}
                        source={{uri:'https://mazlan.dev/android_php/' + banner}}
                    />
                </View>
            );
    }
    return(
        <View style={appStyles.mainContainer}>
            <Loader loading={loading} />
            
            {/* <View style={appStyles.container}>
                
                <Button title="Create Post" onPress={() => navigation.navigate("CreatePost")}/>
                <Button title="Log Out" onPress={LogOut}/>
            </View> */}
            {/* <MaterialIcons 
                name="add"
                size={24}
                style={styles.btnIcon}
                onPress={() => navigation.navigate("CreatePost")}
            /> */}
            <View style={{flex:1, alignSelf:'stretch'}}>
                <FlatList 
                    style={{ flex:1, backgroundColor:'#e0dde9'}}
                    data={posts}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => navigation.navigate("ViewPost", item)}>
                                
                            <Card>
                                <View style={{flex:1, alignSelf:'stretch'}} >
                                    <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                        {/* <Image
                                                style={{width:40, height:40, marginVertical:10, borderRadius:20}}
                                                source={{uri:'https://mazlan.dev/' + item.avatar}}
                                            /> */}
                                        <View>
                                            {/* <Text style={{marginHorizontal:10, fontSize:17}}>{item.nickname}</Text> */}
                                            <Text style={{marginHorizontal:10, fontSize:10}}>@{item.date}</Text>
                                        </View>
                                        
                                    </View>
                                </View>
                                {viewBanner(item.banner)}
                                <Text style={{marginHorizontal:10}}>{item.caption}</Text>
                            </Card>
                        </TouchableOpacity> 
                     
                    )}
                    keyExtractor={(item) => item.post_id}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    btnIcon:{
        padding:10
    }
})
