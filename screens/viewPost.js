import React, {useState} from "react";
import { StyleSheet,View, Text, Button, Image } from "react-native";
import Loader from '../components/loader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Card from '../components/card';

export default function ViewPost({ route, navigation }){
    const [userId, setUserId] = useState('');
    const [loading, setLoading] = useState(false);
    
    AsyncStorage.getItem('user_id').then((user_id) => setUserId(user_id));

    const handleSubmitPress = () => {
        setLoading(true);
        let dataToSend = {post_id: route.params.post_id, user_id:userId};
        let formBody = [];
        for (let key in dataToSend) {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(dataToSend[key]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
    
        fetch('https://mazlan.dev/android_php/deletePost.php', {
          method: 'POST',
          body: formBody,
          headers: {
            //Header Defination
            'Content-Type':
            'application/x-www-form-urlencoded;charset=UTF-8',
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            setLoading(false);
            if(responseJson.success){
                navigation.goBack();
            }
        })
        .catch((error) => {
            console.error(error);
            setLoading(false);
        })
    };
    
    
    function viewBanner(banner){
        if(banner == "")
            return;
        else 
            return (
                <View style={{backgroundColor:'#CCC', flex:1, alignSelf:'stretch', height:200,borderRadius:10, elevation:4}} >

                <Image
                        style={{width:null, height:null, flex:1,borderRadius:10}}
                        source={{uri:'https://mazlan.dev/android_php/' + banner}}
                    />
                </View>
            );
    }
    return(
        <View style={styles.container}>
            <Loader loading={loading} />
            <Card>
                <Text style={{marginHorizontal:10, fontSize:10}}>{route.params.date}</Text>
                {viewBanner(route.params.banner)}
                <Text>{route.params.caption}</Text>
            </Card>
                <Button title="Delete" onPress={handleSubmitPress}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding:30
    }
})