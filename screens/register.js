import React, {useState} from "react";
import { View, Text, TextInput, Button } from "react-native";
import Loader from '../components/loader';
import {appStyles} from '../components/styles';

export default function Register(){
    const [userId, setUserId] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [loading, setLoading] = useState(false);

    const handleSubmitPress = () => {
        if (!userId) {
          alert('Please fill UserId');
          return;
        }
        if (!password) {
          alert('Please fill Password');
          return;
        }
        setLoading(true);
        let dataToSend = {user_id: userId, email: email,  password: password,  password2: password2};
        let formBody = [];
        for (let key in dataToSend) {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(dataToSend[key]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
    
        fetch('https://mazlan.dev/android_php/login.php', {
          method: 'POST',
          body: formBody,
          headers: {
            //Header Defination
            'Content-Type':
            'application/x-www-form-urlencoded;charset=UTF-8',
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            
            setLoading(false);
        })
        .catch((error) => {
            console.error(error);
            setLoading(false);
        })
    };

    return(
        <View style={appStyles.container}>
            <Loader loading={loading} />
            <Text style={appStyles.textLogo}>ToDo List</Text>
            <TextInput 
                placeholder="User ID" 
                style={appStyles.textInput}
                onChangeText={(text) => {setUserId(text)}}/>

            <TextInput 
                placeholder="Email" 
                style={appStyles.textInput}
                onChangeText={(text) => {setEmail(text)}}/>

            <TextInput 
                placeholder="Password" 
                style={appStyles.textInput}
                onChangeText={(text) => {setPassword(text)}}/>

            <TextInput 
                placeholder="Re-Type Password" 
                style={appStyles.textInput}
                onChangeText={(text) => {setPassword2(text)}}/>

            <View style={appStyles.button}>
                <Button title="Register" onPress={handleSubmitPress}/>
            </View>
        </View>
    )
}

