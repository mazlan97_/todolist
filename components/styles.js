import { StyleSheet } from "react-native";

export const appStyles = StyleSheet.create({
    mainContainer:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#e0dde9',
        justifyContent:'center',
    },
    container:{
        padding:30,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'stretch'
    },
    textLogo:{
        fontWeight:'bold',
        color: '#775bc9',
        fontSize:30,
        margin:15,
        alignItems:'center',
    },
    textInput:{
        borderWidth:1,
        borderColor:'#777',
        borderRadius:5,
        paddingVertical:9,
        paddingHorizontal:15,
        margin:5,
        alignSelf:'stretch'
    },
    button:{
        alignSelf:'stretch',
        marginHorizontal:5,
        marginVertical:15,
        borderRadius:10
    }
})