import React from "react";
import { StyleSheet, View } from "react-native";

export default function Card(props){
    return (
        <View style={styles.card}>
            <View>
                {props.children}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card:{
        // flex:1,
        borderRadius:10,
        elevation: 3,
        backgroundColor:'#FFF',
        shadowOffset:{width:1, height:1},
        shadowColor:'#333',
        shadowOpacity:0.3,
        marginHorizontal:15,
        marginVertical:5,
        padding:10
    }
})