import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/home";
import CreatePost from "../screens/createPost";
import ViewPost from "../screens/viewPost";

export default function MainStack(){
    
    const Stack = createNativeStackNavigator();

    return(
            <Stack.Navigator initialRouteName="Home"
            screenOptions={{headerTitleAlign:'center', headerStyle:{backgroundColor:"#775bc9"}, headerTintColor:"white"} }>
                <Stack.Screen 
                    name="Home" 
                    component={Home}
                    options={({ navigation }) => ({
                        title: "ToDo List",
                      })}
                    // options={
                    //     {headerTitle: ({navigation}) => <Header navigation={navigation}/>}
                    // }
                    />

                <Stack.Screen 
                    name="CreatePost" 
                    component={CreatePost}
                    options={
                        {title: "Add List"}
                    } />

                <Stack.Screen 
                    name="ViewPost" 
                    component={ViewPost} />                
                
            </Stack.Navigator>
    )
}