import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "../screens/login";
import Register from "../screens/register";

export default function AuthStack(){
    
    const Stack = createNativeStackNavigator();
    return(
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen 
                    name="Login" 
                    component={Login} 
                    options={{headerShown: false}}/>

                <Stack.Screen 
                    name="Register" 
                    component={Register} />
                
            </Stack.Navigator>
    )
}